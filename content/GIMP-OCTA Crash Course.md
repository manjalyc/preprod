---
title: "GIMP-OCTA Crash Course Notes"
date: 2024-11-29
author: "Cyriac Manjaly"
description: "Notes"
hideSummary: true
searchHidden: true
UseHugoToc: true
ShowReadingTime: false
theme: light
---

# Setup
- [ ] Windows > Single-Window Mode
	- [ ] Orientation: Tools on Left, Layers on Right
- [ ] Open File (`CTRL-O`)
- [ ] Save as .xcf (GIMP File)
- [ ] Setup RGB 
	- [ ] Image > Mode > RGB
- [ ] Lock Pixels of Layer
- [ ] Colors
	- [ ] R: Arteries
	- [ ] B: Veins
	- [ ] G: Unknown
	- [ ] `press x to alternate between colors`
# Layers - Basic
- [ ] Make 2 new layers, arteries and veins
- [ ] The Selected Layer is what you are acting on

# Selection - Basic
- [ ] Select by Color Tool
	- [ ] `SHIFT-O`
	- [ ] **Must Disable all checkboxes**
		- [ ] **especially antialiasing and transparent areas**
	- [ ] **Enable Draw Mask**
	- [ ] Explain Threshold, lower = tighter, higher = looser
- [ ] Hide/Show Selections Lines: `CTRL-T`
- [ ] Select an Artery
# Pencil
- [ ] Default is paintbrush, right-click
	- [ ] `n selects the pencil`
	- [ ] Change Brush to Pixel
	- [ ] **UNCHECK ALL BOXES**
- [ ] **Make sure opacity is 100%**
	- [ ] Tricky later
- [ ] Select Artery Layer to draw on it
- [ ] `CTRL-ALT + SCROLL UP/DOWN` to change brush size
	- [ ] Note ALT alone will mess up opacit
# Selection - Advanced
- [ ] When selecting, make sure the active layer is always the original image
- [ ] Add to current selection
- [ ] Note CTRL-Z undoes the last action
- [ ] Subtract from current selection
- [ ] Intersect with current selection
# Mistakes - Eraser
- [ ] `SHIFT-E` for Eraser
	- [ ] **UNCHECK ALL BOXES**
	- [ ] **ENSURE HARD EDGE IS CHECKED**
# Unknown Vessels
- [ ] Make a layer for every unknown vessel
- [ ] Use a non red/blue color (Green 00FF00)
# Layers - Advanced
- [ ] Showing/Hiding Layers
- [ ] Quickly map out a crossing vein to show/hide the layers
- [ ] Every unknown vessel should be its own layer
- [ ] Merging Layers: merges down to the next visible layer --> Demonstrate with mistake on wrong layer
# Mistakes - Drew on Wrong Layer (ex: Vein on Artery)
- [ ] Make only the mistake (artery) layer visible
- [ ] Make new layer: Temporary
- [ ] On the mistake (artery) layer, select the mistake region by color
	- [ ] Delete: `DEL`
- [ ] Switch the active layer to the Temporary layer
- [ ] Pencil in the region on the temporary layer
- [ ] Merge it into the Vein layer
	- [ ] Demonstrate merging down visible layer

# General Tips
- [ ] Quickly sketch all veins/arteries first
- [ ] Generally first/second order arteries and veins alternate, but not always
- [ ] Advanced troubleshoot with AI, I like [labs.perplexity.ai](https://labs.perplexity.ai) 
- [ ] **Review of Key Differentiating Features**
	- [ ] **GOOD: 
		- [ ] Arteries are generally surrounded by hypointense (darker/less active) areas than veins
	- [ ] ***KINDA USEFUL**
		- [ ] Arteries do not cross other arteries and veins do not cross other veins, physiologically;~~*
			- [ ] First/second order arteries and veins (the big vessels) do not cross themselves
			- [ ] But higher order ones (smaller vessels) might
		- [ ] Vessels can be traced back proximally and distally to aid in identification.
			- [ ] but tough for capillary beds
			- [ ] If not sure, don't label
		- [ ] Arteries and veins usually alternate near the optic disk before branching out
			- [ ] BUT ONLY USUALLY
	- [ ] ***NOT USEFUL in isolation***
		- [ ] Arteries are brighter in color than veins, veins are darker
		- [ ] Central reflex is wider in arteries than veins
	- [ ] **BAD**
		- [ ] Arteries are thinner than neighboring veins
			- [ ] Just not meaningfully true
