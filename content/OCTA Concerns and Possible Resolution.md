---
title: "OCTA Concerns and Possible Resolution"
date: 2024-11-29
author: "Cyriac Manjaly"
description: "OCTA Concerns and Possible Resolution"
hideSummary: false
searchHidden: true
UseHugoToc: true
ShowReadingTime: false
theme: light
---

- Note I know I've mislabeled some arteries and some veins, please don't take the sample image labels as a ground truth, I just want to explore concerns as I messed with the sample image

|               | **OCTA500** Dataset              | **Sample Image from our Dataset**                      |
| ------------- | -------------------------------- | ------------------------------------------------------ |
| **Reference** | ![](</preprod/Attachments/10001.2.6mm.bmp>) | ![](</preprod/Attachments/Pasted image 20241129080829.png>)       |
| **Arteries**  | ![](</preprod/Attachments/10500.1.A.bmp>)   | ![](</preprod/Attachments/Sample Arteries, some mislabeled.png>)  |
| **Veins**     | ![](</preprod/Attachments/10001.3.V.bmp>)   | ![](</preprod/Attachments/Sample Veins, some mislabeled.png>)     |


# Issues Identifying Arteries vs. Veins

### **Rules that don't work for the sample image**
1. Arteries are brighter in color than veins
	- Does not work with the sample image because all vessels are plain white, whereas the OCTA500 Dataset has effective grayscale
	- Rule does not always apply
2. Arteries are thinner than neighboring veins
	- The rule only applies a touch more than 50% of the time making it impractical
3. **Arteries do not cross other arteries and veins do not cross other veins, physiologically**
	- **This does not apply in a practical manner beyond the first/second orders (ie. fail at venules & arterioles)**
		- Encountered many times throughout this process
			- Led to me mislabeling arteries/veins initially until I realized
	- Ex: These 3 intersections cannot follow this rule with any assignment 

| Reference Image with example of bad intersections | Zoomed In on Intersection<br>*These 3 intersections break this rule* |     |
| ------------------------------------------------- | -------------------------------------------------------------------- | --- |
| ![](</preprod/Attachments/Pasted image 20241129082250.png>)  | ![](</preprod/Attachments/Pasted image 20241129082049.png>)                     |     |

4. The central reflex is wider in arteries than veins
	- There is no visible central reflex with these images
### Rules that do work, but...
1. Vessels can be traced back proximally and distally to aid in identification.
	- Yes, but traceability beyond the first/second order into the capillary bed is:
		1. basically impossible, there are so many crossings/overlaps it becomes fruitless
		2. ridiculously time-consuming, to trace them out in the sample image would easily have taken me 10 hours and I wouldn't even be confident in my own accuracy
2. Arteries and veins usually alternate near the optic disk before branching out
	- But only usually
### Rules that work (I think)
1. The presence of surrounding hypointense areas, represent-ing the capillary-free zone, are associated with arteries
	- Need someone to verify this on the sample image (note not on my mislabeled arteries)
	- I wish I had started with this rule first and ignored the other rules

# Major Concerns
- Traceability beyond the first/second order, at least in the sample image, is not possible (at least I cannot do it), there is too much of a mess and too many rules get broken
	- It is also ridiculously time-consuming, I estimate one image could take 5-10 hours to effectively trace out all capillaries, and even then:
		- I also have no confidence in my accuracy with this level of resolution

# One Possible Potential Solution - Flood Zones

| Example(Incomplete) Green(Artery) Flood Zone     | Example(Incomplete) Purple(Vein) Flood Zone       |
| ------------------------------------------------ | ------------------------------------------------- |
| ![](</preprod/Attachments/Pasted image 20241129085105.png>) | ![](</preprod/Attachments/Pasted image 20241129085127.png>)  |
- Instead of tracing capillaries manually due to accuracy and precision limitations explained above, you could consider everything between branches of a known artery to be arterioles (and veins --> venules). Then with some technical work I'll expand on below, you could resolve
	- Possible Technical workflow
		1. Create 4 bitmaps total
			- Ground Truth Arteries
			- Ground Truth Veins
			- Suspected Arterioles
				- Progamatically *subtract* the ground truth arteries bitmap to be left with only suspected possible arterioles
					- I can write this
			- Suspected Venules
				- Progamatically *subtract* the ground truth venules bitmap to be left with only suspected possible arterioles
					- I can write this
		2. Resolution of suspected arterioles vs. venules
			- Programatically resolve conflicts (bitmap overlaps) via distance to nearest ground truth artery/vein
				- I can write this
		3. Modification of reward functions when training the model
			- Adapt the reward function to not overly penalize or reward suspected arteriole/venule matches while maintaining the same established function for ground truth matches.
